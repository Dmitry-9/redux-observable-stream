##### Installation
```
$ yarn install
```

##### Usage
```shell
$ yarn server
```
Starts BE-server and wait for websocket connection

```shell
$ yarn start
```
Starts dev server on http://localhost:8080

It should be looking like this:
```url
https://youtu.be/Ib38Eg2GSF4
```

##### TODO guide
```url
app/src/factors/readme.md
```

#### Trouble shooting
```
Error: listen EADDRINUSE :::4001...
```
Make sure that ports 4001 and 8080 is not already used bu some other application.
```shell
$ netstat -ltnp | grep -w ':4001'
```
