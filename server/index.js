const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const bunyan = require('bunyan');
const { of, from } = require('rxjs');
const { map, repeat, concatMap, delay } = require('rxjs/operators');
const { startValues, units, categories, port } = require('../app/config');
const { shiftValue, rareCase, rareLongDelay } = require('../app/src/utils');

const log = bunyan.createLogger({ name: 'redux-observable-stream' });
const app = express();

const fakeApi = (ev, index, socket) => {
  return from([startValues[index]])
    .pipe(
      concatMap(t => of(t).pipe(delay(100 + rareLongDelay()))),
      map(x => (rareCase() ? x + shiftValue() : x)),
      map(x => Number(x.toFixed(1))),
      repeat()
    )
    .subscribe(x => socket.emit(ev, `${x} ${units[index]}`));
};

const server = http.createServer(app);
const io = socketIo(server);

io.on('connection', socket => {
  log.info('New client connected');

  const allObservers = categories.map((ev, i) => fakeApi(ev, i, socket));

  socket.on('disconnect', () => {
    log.info('Client disconnected');
    allObservers.map(o => o.unsubscribe());
  });
});

server.listen(port, () => log.info(`Listening on port ${port}`));
