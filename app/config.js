module.exports = {
  port: process.env.PORT || 4001,
  categories: ['temperature', 'airPressure', 'humidity'],
  displayObjShape: {
    temperature: '',
    airPressure: '',
    humidity: '',
  },
  units: ['c', 'mbar', '%'],
  startValues: [18, 1013.2, 40],
  endpoint: 'http://127.0.0.1:4001',
  absenceOfResponse: 'N/A',
};
