const plusOrMinus = () => (Math.random() < 0.5 ? -1 : 1);

const utils = {
  shiftValue: () => plusOrMinus() * Math.random() * 1.5,
  rareCase: () => Math.random() * 10 > 7,
  rareLongDelay: () => (utils.rareCase() ? Math.random() * 2000 : 0),
};

module.exports = utils;
