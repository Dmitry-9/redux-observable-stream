import React from 'react';
import ReactDOM from 'react-dom';
// import { createEpicMiddleware } from 'redux-observable';
// import { createStore, applyMiddleware } from 'redux';
// import { Provider } from 'react-redux';
// import rootReducer from './reducers';
// import rootEpic from './epics';

import App from './components/App';

// const epicMiddleware = createEpicMiddleware();
// const store = createStore(
//   rootReducer,
//   applyMiddleware(epicMiddleware)
// );
// epicMiddleware.run(rootEpic);

// ReactDOM.render(
//   <Provider store={store}><App /></Provider>,
//   document.getElementById('root')
// );

/**
 * TODO IMPLEMENT REDUCERS, EPICS, STORE
 */
ReactDOM.render(<App />, document.getElementById('root'));

module.hot.accept();
