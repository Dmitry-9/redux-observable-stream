import { combineReducers } from 'redux';
import emitDisplayReducer from './emit-display';
import tempretureReducer from './tempreture';
import humidityReducer from './humidity';
import airPressureReducer from './air-pressure';

export default combineReducers({
  tempretureReducer,
  humidityReducer,
  airPressureReducer,
  emitDisplayReducer,
});
