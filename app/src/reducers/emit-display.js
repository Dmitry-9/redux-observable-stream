import A from '../actions';
import { displayObjShape, categories } from '../../config';

const emitDisplayReducer = (state = {}, action) => {
  switch (action.type) {
    case A.SHOULD_EMIT:
      return {
        ...state,
        [A.SHOULD_EMIT]: true,
        displayObj: update(action),
      };

    default:
      return {
        ...state,
        [A.SHOULD_EMIT]: false,
      };
  }
};

const update = ({ displayObj }) => {
  const clone = { ...displayObjShape };
  categories.forEach(key => (clone[key] = displayObj[key]));
  return clone;
};

export default emitDisplayReducer;
