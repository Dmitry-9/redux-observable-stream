import A from '../actions';

const airPressureReducer = (state = {}, action) => {
  switch (action.type) {
    case A.AIRPRESSURE:
      return {
        ...state,
        [A.AIRPRESSURE]: action[A.AIRPRESSURE],
      };

    default:
      return state;
  }
};

export default airPressureReducer;
