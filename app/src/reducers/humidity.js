import A from '../actions';

const humidityReducer = (state = {}, action) => {
  switch (action.type) {
    case A.HUMIDITY:
      return {
        ...state,
        [A.HUMIDITY]: action[A.HUMIDITY],
      };

    default:
      return state;
  }
};

export default humidityReducer;
