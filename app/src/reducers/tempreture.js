import A from '../actions';

const tempretureReducer = (state = {}, action) => {
  switch (action.type) {
    case A.TEMPERATURE:
      return {
        ...state,
        [A.TEMPERATURE]: action[A.TEMPERATURE],
      };

    default:
      return state;
  }
};

export default tempretureReducer;
