#### **\*** REQUIREMENTS **\***

##### If a value is not received from a specific system for more than 1000ms

```javascript
const timeStrideNAFactor = 1000;

const reactOnLowNetDataFrequncyTemp = action$ =>
  action$.pipe(
    ofType('*_DATA_RECEIVED_FROM_BE'),
    timer(timeStrideNAFactor),
    switchMap(action => {
      return updateCategoryActionCreate('SHOULD_EMIT_DISPLAY_OBJECT', '*_NA');
    })
  );
```

##### Display object should not be emitted more often than every 100ms

```javascript
ations$.pipe(
  ofType('*_DATA_RECEIVED_FROM_BE'),
  debounceTime(100),
  switchMap(action => {
    return actionCreate('*_frequency filter passed');
  })
);
```

##### Display object should only be emitted when one of the systems sends a new value

```javascript
let prevVal;

ations$.pipe(
  ofType('*_frequency filter passed')
  filter(value => value !== prevVal),
  map(value => prevVal = value),
  mapTo(value => actionCreate('*_new value filter passed'))
);
```

##### observable that, when subscribed to, emits a display object containing the latest value of all three systems, to be consumed by the dashboard

```javascript
zip$(
  ations$.of('temperature_new value filter passed'),
  ations$.of('airpressure_new value filter passed'),
  ations$.of('humidity_new value filter passed')
).pipe(
  takeLast(),
  mapTo(value => actionCreate('SHOULD_EMIT_DISPLAY_OBJECT'))
);
```
