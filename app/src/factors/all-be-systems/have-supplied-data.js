import is from '@sindresorhus/is';
import { absenceOfResponse } from '../../../config';
// TODO instead emit socket event and on the BE write to debug.log
if (!is.string(absenceOfResponse)) throw Error('Err code 2272727');

/**
 * All 3 systems must emit at least one value
 *  before 1 display object is ever sent to the dashboard.
 *  @param {Obgect} displayObj
 * * {string} displayObj.temperature
 * * {string} displayObj.airPressure
 * * {string} displayObj.humidity
 * @returns {boolean} should sent to the dashboard or not
 */
let isRequirementSatisfied = false;

const factors = {
  isSetFull: obj => {
    if (!is.object(obj)) return false;
    if (isRequirementSatisfied) return true;

    isRequirementSatisfied = [
      Object.keys(obj).length === 3,
      Object.values(obj).every(Boolean),
      !Object.values(obj).includes(absenceOfResponse),
    ].every(Boolean);

    return false;
  },
};

export default factors;
