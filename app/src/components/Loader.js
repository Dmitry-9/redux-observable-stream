import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2,
  },
});

const CircularIndeterminate = props => {
  const { classes } = props;
  return (
    <div>
      <CircularProgress size={100} className={classes.progress} />
    </div>
  );
};

CircularIndeterminate.propTypes = {
  classes: PropTypes.object.isRequired,
}; // todo PropTypes.shape

export default withStyles(styles)(CircularIndeterminate);
