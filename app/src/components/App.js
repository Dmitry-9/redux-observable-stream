import React, { Component } from 'react';
import PropTypes from 'prop-types';
import socketIOClient from 'socket.io-client';
import Dashboard from './Dashboard';
import Loader from './Loader';
// import A from '../actions' // TODO use string A.SHOULD_EMIT as state key <gate away>
import forAllSystems from '../factors/all-be-systems/have-supplied-data';

const { categories, endpoint } = require('../../config');

// TODO use react hooks instead of state full component
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayObj: props.displayObj, // TODO prevDisplayObj: props.displayObj,
    };
  }

  componentDidMount() {
    const socket = socketIOClient(endpoint);
    categories.map(ev => {
      socket.on(ev, data =>
        this.setState({ displayObj: { ...this.state.displayObj, [ev]: data } })
      );
    });
  }

  render() {
    // consult with
    // TODO const displayObjToRender = this.props[A.SHOULD_EMIT]
    //   ? this.props.displayObj
    //   : this.state.prevDisplayObj;

    // prepare for next iteration
    // TODO this.setState({prevDisplayObj: this.props.displayObj})
    return (
      <div style={style}>
        {// for this simple case epics is overkill
        forAllSystems.isSetFull(this.state.displayObj) ? ( // TODO pass displayObjToRender
          <Dashboard displayObj={this.state.displayObj} /> // TODO pass displayObjToRender
        ) : (
          <Loader />
        )}
      </div>
    );
  }
}

const style = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  minHeight: '100vh',
};

App.propTypes = {
  displayObj: PropTypes.shape({
    temperature: PropTypes.string.isRequired,
    airPressure: PropTypes.string.isRequired,
    humidity: PropTypes.string.isRequired,
  }),
};

export default App;
