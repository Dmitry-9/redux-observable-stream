import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';

const is = require('@sindresorhus/is');

const styles = theme => ({
  root: {
    flexGrow: 1,
    overflow: 'hidden',
    padding: `0 ${theme.spacing.unit * 3}px`,
  },
  paper: {
    maxWidth: 400,
    margin: `${theme.spacing.unit}px auto`,
    padding: theme.spacing.unit * 2,
  },
});

const AutoGridNoWrap = props => {
  const { classes, displayObj } = props;
  const styleNum = {
    display: 'flex',
    padding: '0.5em',
    justifyContent: 'center',
    color: '#fb3e3e',
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        {Object.keys(displayObj).map((key, i) => {
          return (
            <div key={i}>
              <Paper className={classes.paper}>
                <Grid container wrap="nowrap" spacing={16}>
                  {is.string(key[0]) && (
                    <Grid item>
                      <Tooltip title={key.toUpperCase()} placement="right">
                        <Avatar>{key[0].toUpperCase()}</Avatar>
                      </Tooltip>
                    </Grid>
                  )}
                  <Grid item xs>
                    <Typography variant={'title'}>
                      <div style={styleNum}>{displayObj[key]}</div>
                    </Typography>
                  </Grid>
                </Grid>
              </Paper>
            </div>
          );
        })}
      </Paper>
    </div>
  );
};

AutoGridNoWrap.propTypes = {
  classes: PropTypes.object.isRequired,
  displayObj: PropTypes.object.isRequired,
}; // TODO PropTypes.shape

export default withStyles(styles)(AutoGridNoWrap);
